loadEvents();

function loadEvents(){
  document.querySelector('form').addEventListener('submit',submit);
  document.querySelector('ul').addEventListener('click',deleteOrTick);

}
// Envio de datos
function submit(e){
  e.preventDefault();
  let taskList;
  let input = document.querySelector('input');
  if(input.value != '')
    addTask(input.value);
  input.value = '';
}

// Agregar Tareas
function addTask(task){
  let ul = document.querySelector('ul');
  let li = document.createElement('li');
  li.innerHTML = `<input type="checkbox"><label>${task}</label><button class="delete"><i class="far fa-trash-alt"></i></button>`;
  ul.appendChild(li);
  document.querySelector('.tasksBoard').style.display = 'block';
}


// Borrar Marca
function deleteOrTick(e){
  if(e.target.className == 'delete')
    deleteTask(e);
  else {
    tickTask(e);
  }
}

// Borrar una tarea
function deleteTask(e){
  let remove = e.target.parentNode;
  let parentNode = remove.parentNode;
  parentNode.removeChild(remove);
}

// Marcar tarea
function tickTask(e){
  const task = e.target.nextSibling;
  if(e.target.checked){
    task.style.textDecoration = "line-through";
    task.style.color = "black";
  }else {
    task.style.textDecoration = null;
    task.style.color = "Black";
  }
}
